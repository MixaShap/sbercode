# README

## Описание задачи

Построение системы классификации отзывов пользователей в AppStore / Google Play

## Технические особенности

Верхнеуровнево решение оформлено в web-service. И уже готово встраиваться в любую микросервисную архзитектуру.

Под капотом веб приложения в первую очередь работает набор правил, который выявляет последовательности ключевых слов в отзыве и возвращает список команд-разработчиков, которые связаны с этим отзывом.
Так же проходит поиск ключевых слов, характеризующих тональность отзыва. Большее внимание уделяется негативным отзывам.

Только после обработки по правилам подключаются алгоритмы машинного обучения. 

Решение реализует кластеризацию отзывов и визуализацию основных слов в каждом кластере.

## Дополнительные ссылки

1. Мы опирались в первую очередь на специфику данных. Контекст данных очень особенный, 
поэтому в первую очередь мы проанализировали корни слов в отзывах.
1. Не удивительно, но количество знаков вопросов и восклецательных знаков уже дают много информации об эмоциональном окрасе отзыва,
 поэтому в ходе фичаинжениринга мы подсчитывали количество этих символов встречающихся в отзыве
  и количество груп символов от 3 и выше (оказалось, что 10 - это максимальное количество символов)
1. Мы реализовали фичаинжиниринг, базируясь на TFIDF подходе,
 который уже давно зарекомендовал себя, как мощный инструмент в NLP ()фы.
1. Нам не хватает вре

## Контакты

8-915-0666830 simonovyup@gmail.com Юрий
8-977-9629846 mixashapdev@gmail.com Михаил
8-916-6410378 princess.emotion21@gmail.com Марина

# Запуск приложения

- склонируйте репозиторий Unix машину
- в терминале выполните команды `pipenv shell`, `pipenv install`
(если pipenv не установлен, то создайте виртуальное окружение
с python 3.7 и установите туда все библиотеки из списка в Pipfile)
- убедитесь, что в PYTHON_PATH добавлен путь до .../Code/src/
- выполните команду  `python Code/src/web_services/web_service.py port==<PORT>`, 
где `PORT` - номер порта для приложения
- в браузере перейдите по адресу `<host>:<PORT>/new_review?rating=<оценка>&title=<заголовок отзыва>&review=<текс отзыва>&answer=<ответ, если он есть, если его нет, можно пропустить аргумент>`,
где host - IP машины (если сервис запущен локально, то `127.0.0.1` или `0.0.0.0`)
