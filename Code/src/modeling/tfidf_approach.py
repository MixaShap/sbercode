from typing import List

import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

from loader.constants import STOP_WORDS


def get_tfidf_emb(all_texts: pd.Series(), add_stop: List[str]) -> pd.DataFrame:
    r"""Реализация фичаинженера с TFIDF подходом
    в значениях all_texts должны быть обработанные отзывы
    (Например, с помощью функции text_handler)"""
    tfidf_vectorizer = TfidfVectorizer(stop_words=STOP_WORDS + add_stop)
    values = tfidf_vectorizer.fit_transform(all_texts)
    feature_names = tfidf_vectorizer.get_feature_names()
    feature = pd.DataFrame(values.toarray(), columns=feature_names)
    return feature
