from loader.loaders import words_loader, teams_yaml_loader, stop_words_loader

BAD_WORDS = words_loader('bad_words.yaml')
GOOD_WORDS = words_loader('good_words.yaml')
STOP_WORDS = stop_words_loader('stop_words.yaml')

W2T = teams_yaml_loader()
