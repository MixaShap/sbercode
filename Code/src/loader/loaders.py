import pandas
import yaml
from flashtext import KeywordProcessor

from settings import RES_PATH


def words_loader(filename: str):
    path = RES_PATH / filename
    with open(path, encoding='utf-8') as f:
        words = yaml.load(f, Loader=yaml.FullLoader)
    return words


def stop_words_loader(filename: str):
    path = RES_PATH / filename
    with open(path, encoding='utf-8') as f:
        st_w = yaml.load(f, Loader=yaml.FullLoader)
    return st_w


def rev_team_dict(keyword_dict):
    r"""Метод переворач вает словарь"""
    new_dict = {}
    for key, vals in keyword_dict.items():
        for val in vals:
            new_dict[val] = key
    return new_dict


def teams_yaml_loader():
    path = RES_PATH / 'commands.yaml'
    print(path)
    with open(path, encoding='utf-8') as f:
        keyword_dict = yaml.load(f, Loader=yaml.FullLoader)
    word_to_team = rev_team_dict(keyword_dict)
    return word_to_team


def max_csv_loader():
    r"""Загрузка всех отзывов apple и всех отзывов android"""
    apple_filename = RES_PATH / 'reviews/AppStore.csv'
    apple_dataset = pandas.read_csv(apple_filename, sep=',', comment="#", usecols=['Rating', 'Title', 'Review'])
    apple_dataset.columns = ['rating', 'title', 'review']
    # Android_filename = RES_PATH / 'reviews/GP/reviews_reviews_ru.sberbankmobile_201907.csv'
    android_filename = RES_PATH / 'reviews/GP/xlsx.csv'

    android_dataset = pandas.read_csv(android_filename, sep=',', encoding="cp1251",
                                      usecols=['Star Rating', 'Review Title', 'Review Text'], keep_default_na=False)

    android_dataset.columns = ['rating', 'title', 'review']

    android_dataset = android_dataset.drop(
        android_dataset[(android_dataset['title'] == "") & (android_dataset['review'] == "")].index)  # убираем пустые отзывы

    dataset = pandas.concat([apple_dataset, android_dataset])

    # dataset['review'] = dataset['review'].map(lambda x: x.lower())
    # dataset['title'] = dataset['title'].map(lambda x: x.lower())

    return dataset.reset_index(drop=True)


# max_csv_loader()
