import datetime
import inspect
import os
import sys


def get_myself_name() -> str:
    r"""Метод для получения имени сервиса"""
    return inspect.stack()[1].function


def get_port() -> int:
    r"""Получение номера порта из kwargs. По умолчанию используется FLASK_PORT из env"""
    kwargs = {kw[0]: kw[1] for kw in [arg.split('=') for arg in sys.argv[1:] if '=' in arg]}
    if 'port' in kwargs:
        port = int(kwargs['port'])
    else:
        port = os.environ.get('FLASK_PORT')
    return port


def check_is_alive(start_time: datetime.datetime) -> str:
    """Checking the health of the service"""
    live_time = datetime.datetime.now() - start_time
    message = f"I have been living for {live_time}"
    return f'Ololo\n {message}'
