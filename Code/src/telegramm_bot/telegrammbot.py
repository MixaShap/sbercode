import pandas as pd
import telebot
from telebot import types

from analyzer.team_definer import get_team_from_review
from privat import TELEGRAM_API_KEY
from structures import Review
from telegramm_bot.choose_categories import choose_categories

bot = telebot.TeleBot(TELEGRAM_API_KEY)


rating = ''
heading = ''
error = ''
review = ''
is_activated = False


def get_all(local_rating, local_heading, local_review):
    # d = {"Rating": pd.Series([local_rating]), "Title": pd.Series([local_heading]), "Review": pd.Series([local_review])}
    # dataset = pd.DataFrame(d)
    # return choose_categories(dataset)
    review = Review(rating=local_rating, title=local_heading, review=local_review)
    print(get_team_from_review(review=review))
    return get_team_from_review(review=review)


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.from_user.id, 'Добрый день, Вас приветствует SberBot!')
    markup = types.InlineKeyboardMarkup()
    for score in range(1, 6):
        markup.add(types.InlineKeyboardButton(text=str(score), callback_data=str(score)))
    bot.send_message(message.from_user.id, text='Оцените наше приложение "Сбербанк Онлайн"', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(message):
    global rating
    rating = message.data
    global is_activated
    is_activated = True
    print(rating)
    bot.send_message(message.from_user.id, 'Спасибо за честный ответ')
    bot.send_message(message.from_user.id, 'Напишите заголовок к Вашему комментарию')


@bot.message_handler(commands=['error'])
def error_message(message):
    bot.send_message(message.from_user.id, 'Спасибо, отзыв будет отправлен в нужный отдел!')
    global error
    error = message.text
    print(error)


@bot.message_handler(content_types=['text'])
def get_heading(message):
    global is_activated
    if is_activated:
        global heading
        heading = message.text
        print(heading)
        bot.send_message(message.from_user.id, 'Напишите подробный отзыв')
        bot.register_next_step_handler(message, get_review)
    else:
        bot.send_message(message.from_user.id, 'Начните сначала, напишите /start или поставьте оценку, если Вы этого '
                                               'не сделали после нажатия /start')


def get_review(message):
    global review
    global is_activated
    review = message.text
    print(review)
    bot.send_message(message.from_user.id, 'Ваша оценка: '+rating+'\nЗаголовок: '+heading+'\nСодержание отзыва: '+review)
    categories_list = get_all(rating, heading, review)
    categories_str = ''
    for i in categories_list:
        categories_str = categories_str + str(i) + '\n'
        print(str(i))
    bot.send_message(message.from_user.id, 'Спасибо за Ваш отзыв, он будет отправлен в отделы:\n' + categories_str)
    bot.send_message(message.from_user.id, 'Если Ваш отзыв отправлен не в тот отдел введите:\n '
                                           '/error название одного отдела')
    bot.send_message(message.chat.id, 'Если лишний отдел не один, после ввода одного отдела снова нажмите:\n '
                                      '/error название одного отдела')
    is_activated = False


bot.polling()
