from typing import Tuple

from analyzer.speller import spell
from loader.constants import BAD_WORDS, GOOD_WORDS
from structures import Review


def rules_semantic(text: str) -> Tuple[int, int]:
    r"""Метод для посчета хороших и плохих ключевых слов в тексте"""
    bad_word_counter = 0
    good_word_counter = 0
    for bad_word in BAD_WORDS:
        bad_word_counter += text.count(bad_word)
    for good_word in GOOD_WORDS:
        good_word_counter += text.count(good_word)
    return bad_word_counter, good_word_counter


def get_semantic(review: Review) -> bool:
    """Метод для определения семантики
    :param review:
    :return: True если отзыв положительный и
            False если отзыв отрицательный
    """
    review.review = review.review.lower()
    review.title = review.title.lower()
    spelled_text = spell(review.review + review.title)
    bad_word_counter, good_word_counter = rules_semantic(spelled_text)

    return bad_word_counter == 0

