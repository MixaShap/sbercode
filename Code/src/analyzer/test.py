from analyzer.semantic_definer import get_semantic
from analyzer.team_definer import get_team_from_review
from structures import Review


if __name__ == "__main__":
    review = Review(rating=5,
                    title="Хорошо",
                    review="Не работает applepay чат")
    print("get_semantic: ", get_semantic(review=review))
    print('=====')
    print("get_team_from_review: ", get_team_from_review(review=review))
