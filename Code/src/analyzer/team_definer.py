from typing import Set

from analyzer.speller import spell
from loader.constants import W2T
from structures import Review


def get_teams(text: str):
    teams = []
    for kw in W2T:
        if kw in text:
            teams.append(W2T[kw])
    return teams


def get_team_from_review(review: Review) -> Set[str]:
    """
    :param review:
    :return: возвращает список отделов, которых касается данный отзыв
    """
    spelled_text = spell(review.review + review.title)
    command = set(get_teams(spelled_text))
    return command
